# Install script for directory: /home/fs/laitinen/S3/Kupla/qjson/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/fs/laitinen/S3/Kupla/qjson/qjson")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")
  FILE(INSTALL DESTINATION "/home/fs/laitinen/S3/Kupla/qjson/qjson/include/qjson" TYPE FILE FILES
    "/home/fs/laitinen/S3/Kupla/qjson/src/parser.h"
    "/home/fs/laitinen/S3/Kupla/qjson/src/parserrunnable.h"
    "/home/fs/laitinen/S3/Kupla/qjson/src/qobjecthelper.h"
    "/home/fs/laitinen/S3/Kupla/qjson/src/serializer.h"
    "/home/fs/laitinen/S3/Kupla/qjson/src/serializerrunnable.h"
    "/home/fs/laitinen/S3/Kupla/qjson/src/qjson_export.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FOREACH(file
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so.0.7.1"
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so.0"
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  FILE(INSTALL DESTINATION "/home/fs/laitinen/S3/Kupla/qjson/qjson/lib" TYPE SHARED_LIBRARY FILES
    "/home/fs/laitinen/S3/Kupla/qjson/lib/libqjson.so.0.7.1"
    "/home/fs/laitinen/S3/Kupla/qjson/lib/libqjson.so.0"
    "/home/fs/laitinen/S3/Kupla/qjson/lib/libqjson.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so.0.7.1"
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so.0"
      "$ENV{DESTDIR}/home/fs/laitinen/S3/Kupla/qjson/qjson/lib/libqjson.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

